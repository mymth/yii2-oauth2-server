<?php

namespace mymth\oauth2server\repositories;

use Yii;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;

/**
 * Repository class for access token entity
 */
class AccessTokenRepository extends \yii\base\Object implements AccessTokenRepositoryInterface
{
    /**
     * @var string class of access token entity. must be a subclass of yii\db\ActiveRecord
     * that implements League\OAuth2\Server\Entities\AccessTokenEntityInterface
     */
    public $entityClass = 'mymth\oauth2server\entities\AccessToken';

    /**
     * Create a new access token
     *
     * @param ClientEntityInterface  $clientEntity
     * @param ScopeEntityInterface[] $scopes
     * @param mixed                  $userIdentifier
     *
     * @return AccessTokenEntityInterface
     */
    public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null)
    {
        $entityClass = $this->entityClass;

        $accessToken = new $entityClass([
            'client' => $clientEntity,
            'userIdentifier' => $userIdentifier,
        ]);
        foreach ($scopes as $scope) {
            $accessToken->addScope($scope);
        }

        return$accessToken;
    }

    /**
     * Persists a new access token to permanent storage.
     *
     * @param AccessTokenEntity
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity)
    {
        $entityClass = $this->entityClass;

        if ($accessTokenEntity instanceof $entityClass) {
            $accessToken = $accessTokenEntity;
        } else {
            $accessToken = new $entityClass([
                'identifier' => $accessTokenEntity->getIdentifier(),
                'client' => $accessTokenEntity->getClient(),
                'userIdentifier' => $accessTokenEntity->getUserIdentifier(),
                'expiryDateTime' => $accessTokenEntity->getExpiryDateTime(),
            ]);
            foreach ($accessTokenEntity->getScopes() as $scope) {
                $accessToken->addScope($scope);
            }
        }
        $accessToken->save();
    }

    /**
     * Revoke an access token.
     *
     * @param string $tokenId
     */
    public function revokeAccessToken($tokenId)
    {
        $entityClass = $this->entityClass;

        $entityClass::updateAll(['revoked' => true], ['id' => $tokenId]);
    }

    /**
     * Check if the access token has been revoked.
     *
     * @param string $tokenId
     *
     * @return bool Return true if this token has been revoked
     */
    public function isAccessTokenRevoked($tokenId)
    {
        $entityClass = $this->entityClass;

        return $entityClass::find()->where(['id' => $tokenId, 'revoked' => true])->exists();
    }
}
