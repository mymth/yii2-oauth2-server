<?php

namespace mymth\oauth2server\repositories;

use Yii;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;

/**
 * Repository class for refresh token entity
 */
class RefreshTokenRepository extends \yii\base\Object implements RefreshTokenRepositoryInterface
{
    /**
     * @var string class of refresh token entity. must be a subclass of yii\db\ActiveRecord
     * that implements League\OAuth2\Server\Entities\RefreshTokenEntityInterface
     */
    public $entityClass = 'mymth\oauth2server\entities\RefreshToken';

    /**
     * Creates a new refresh token
     *
     * @return RefreshTokenEntityInterface
     */
    public function getNewRefreshToken()
    {
        $entityClass = $this->entityClass;

        return new $entityClass();
    }

    /**
     * Create a new refresh token_name.
     *
     * @param RefreshTokenEntityInterface $refreshTokenEntity
     */
    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntity)
    {
        $entityClass = $this->entityClass;

        if ($refreshTokenEntity instanceof $entityClass) {
            $refreshToken = $refreshTokenEntity;
        } else {
            $refreshToken = new $entityClass([
                'identifier' => $refreshTokenEntity->getIdentifier(),
                'accessToken' => $refreshTokenEntity->getAccessToken(),
                'expiryDateTime' => $refreshTokenEntity->getExpiryDateTime(),
            ]);
        }
        $refreshToken->save();
    }

    /**
     * Revoke the refresh token.
     *
     * @param string $tokenId
     */
    public function revokeRefreshToken($tokenId)
    {
        $entityClass = $this->entityClass;

        $entityClass::updateAll(['revoked' => true], ['id' => $tokenId]);
    }

    /**
     * Check if the refresh token has been revoked.
     *
     * @param string $tokenId
     *
     * @return bool Return true if this token has been revoked
     */
    public function isRefreshTokenRevoked($tokenId)
    {
        $entityClass = $this->entityClass;

        return $entityClass::find()->where(['id' => $tokenId, 'revoked' => true])->exists();
    }
}
