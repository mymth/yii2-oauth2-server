<?php

namespace mymth\oauth2server\repositories;

use Yii;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use mymth\oauth2server\Module;

/**
 * Repository class for scope token entity
 */
class ScopeRepository extends \yii\base\Object implements ScopeRepositoryInterface
{
    /**
     * @var string class of scope entity. must be a class that implements
     * League\OAuth2\Server\Entities\ScopeEntityInterface
     */
    public $entityClass = 'mymth\oauth2server\entities\Scope';

    private $_availableScopes;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->_availableScopes = Module::getInstance()->availableScopes;
    }

    /**
     * Return information about a scope.
     *
     * @param string $identifier The scope identifier
     *
     * @return ScopeEntityInterface
     */
    public function getScopeEntityByIdentifier($identifier)
    {
        $entityClass = $this->entityClass;

        if (in_array($identifier, $this->_availableScopes)) {
            return new $entityClass($identifier);
        }
    }

    /**
     * Given a client, grant type and optional user identifier validate the set of scopes requested are valid and optionally
     * append additional scopes or remove requested scopes.
     *
     * @param ScopeEntityInterface[] $scopes
     * @param string                 $grantType
     * @param ClientEntityInterface  $clientEntity
     * @param null|string            $userIdentifier
     *
     * @return ScopeEntityInterface[]
     */
    public function finalizeScopes(
        array $scopes,
        $grantType,
        ClientEntityInterface $clientEntity,
        $userIdentifier = null
    )
    {
        $final = [];
        foreach ($scopes as $scope) {
            if (in_array($scope->getIdentifier(), $this->_availableScopes)) {
                $final[] = $scope;
            }
        }

        return $final;
    }
}
