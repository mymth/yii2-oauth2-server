<?php

namespace mymth\oauth2server\repositories;

use Yii;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Entities\AuthCodeEntityInterface;

/**
 * Repository class for authorization code entity
 */
class AuthCodeRepository extends \yii\base\Object implements AuthCodeRepositoryInterface
{
    /**
     * @var string class of auth code entity. must be a subclass of yii\db\ActiveRecord
     * that implements League\OAuth2\Server\Entities\AuthCodeEntityInterface
     */
     public $entityClass = 'mymth\oauth2server\entities\AuthCode';

    /**
     * Creates a new AuthCode
     *
     * @return AuthCodeEntityInterface
     */
    public function getNewAuthCode()
    {
        $entityClass = $this->entityClass;

        return new $entityClass();
    }

    /**
     * Persists a new auth code to permanent storage.
     *
     * @param AuthCodeEntityInterface $authCodeEntity
     */
    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity)
    {
        $entityClass = $this->entityClass;

        if ($authCodeEntity instanceof $entityClass) {
            $authCode = $authCodeEntity;
        } else {
            $authCode = new $entityClass([
                'identifier' => $authCodeEntity->getIdentifier(),
                'client' => $authCodeEntity->getClient(),
                'userIdentifier' => $authCodeEntity->getUserIdentifier(),
                'expiryDateTime' => $authCodeEntity->getExpiryDateTime(),
            ]);
            foreach ($authCodeEntity->getScopes() as $scope) {
                $authCode->addScope($scope);
            }
        }
        $authCode->save();
    }

    /**
     * Revoke an auth code.
     *
     * @param string $codeId
     */
    public function revokeAuthCode($codeId)
    {
        $entityClass = $this->entityClass;

        $entityClass::updateAll(['revoked' => true], ['id' => $codeId]);
    }

    /**
     * Check if the auth code has been revoked.
     *
     * @param string $codeId
     *
     * @return bool Return true if this code has been revoked
     */
    public function isAuthCodeRevoked($codeId)
    {
        $entityClass = $this->entityClass;

        return $entityClass::find()->where(['id' => $codeId, 'revoked' => true])->exists();
    }
}
