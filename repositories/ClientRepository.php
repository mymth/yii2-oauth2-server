<?php

namespace mymth\oauth2server\repositories;

use Yii;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;

/**
 * Repository class for client entity
 */
class ClientRepository extends \yii\base\Object implements ClientRepositoryInterface
{
    /**
     * @var string class of client entity. must be a subclass of yii\db\ActiveRecord
     * that implements League\OAuth2\Server\Entities\ClientEntityInterface
     */
    public $entityClass = 'mymth\oauth2server\entities\Client';

    /**
     * Get a client.
     *
     * @param string      $clientIdentifier   The client's identifier
     * @param string      $grantType          The grant type used
     * @param null|string $clientSecret       The client's secret (if sent)
     * @param bool        $mustValidateSecret If true the client must attempt to validate the secret if the client
     *                                        is confidential
     *
     * @return ClientEntityInterface
     */
    public function getClientEntity($clientIdentifier, $grantType, $clientSecret = null, $mustValidateSecret = true)
    {
        $entityClass = $this->entityClass;

        $client = $entityClass::findOne($clientIdentifier);
        if ($client) {
            // client w/o redirect uri is not a target of authcode or implicit grant
            if (!$client->getRedirectUri() && in_array($grantType, ['authorization_code', 'implicit'])) {
                return;
            }
            if ($mustValidateSecret) {
                if (!Yii::$app->getSecurity()->compareString($client->secret, $clientSecret)) {
                    return;
                }
            }
        }

        return $client;
    }
}
