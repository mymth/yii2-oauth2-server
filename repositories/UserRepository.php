<?php

namespace mymth\oauth2server\repositories;

use Yii;
use yii\base\InvalidConfigException;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use mymth\oauth2server\entities\UserEntityInterface;
use mymth\oauth2server\Module;

/**
 * Repository class for user entity
 */
class UserRepository extends \yii\base\Object implements UserRepositoryInterface
{
    /**
     * @var string class of user entity.
     * automatically set to applications's $identityClass
     */
    public $entityClass;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!$this->entityClass) {
            $this->entityClass = Yii::$app->getUser()->identityClass;
        }
    }

    /**
     * Get a user entity.
     *
     * @param string                $username
     * @param string                $password
     * @param string                $grantType    The grant type used
     * @param ClientEntityInterface $clientEntity
     *
     * @return UserEntityInterface
     */
    public function getUserEntityByUserCredentials(
        $username,
        $password,
        $grantType,
        ClientEntityInterface $clientEntity
    )
    {
        $entityClass = $this->entityClass;

        $callableName = null;
        if (!(new $entityClass()) instanceof UserEntityInterface) {
            $msg = ' must implement '. UserEntityInterface::class;
            throw new InvalidConfigException($entityClass . $msg);
        }

        $user = $entityClass::findByUsername($username);
        if ($user && !$user->validatePassword($password)) {
            $user = null;
        }

        return $user;
    }
}
