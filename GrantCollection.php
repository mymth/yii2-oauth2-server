<?php

namespace mymth\oauth2server;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * DI container that provides a singleton instance of each grant type class.
 * To customize grant type definition, you can set your definition into the config array
 * of this class in the following format:
 * ```
 *   _grant_type_ => [
 *       'class' => _GrantTypeClass_,
 *       'params' => _array of the arguments passed to the consructor_
 *   ],
 * ```
 * The `params` option can be an anonymous function that returns an array of the
 * constructor arguments.
 * ```
 *       'params' => function ($module, $repos) {
 *           // $module - oauth2 server module instance
 *           // $repos  - instance of RepositoryCollection
 *
 *           //...
 *
 *           return [$arg1, $arg2, ...];
 *       },
 * ```
 * You can also add your custom grant type by setting your own grant_type keyword and
 * GrantType class.
 */
class GrantCollection extends \yii\di\Container
{
    private $_defsConfig = [];
    private $_params = [];

    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        foreach (array_keys($config) as $key) {
            if (!$this->canSetProperty($key)) {
                $cf = $config[$key];
                if (!is_array($cf)) {
                    $cf = ['class' => $cf];
                }
                $this->_defsConfig[$key] = $cf;
                unset($config[$key]);
            }
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $defsConfig = ArrayHelper::merge($this->defaultConfig(), $this->_defsConfig);
        $module = Module::getInstance();
        $repos = $module->getRepositories();

        foreach ($defsConfig as $key => $cf) {
            if (isset($cf['params'])) {
                $params = $cf['params'];
                if (is_callable($params)) {
                    $params = call_user_func($params, $module, $repos);
                }
                $this->_params[$key] = $params;
                unset($cf['params']);
            }
            $this->setSingleton($key, $cf);
        }
    }

    /**
     * Returns the default configuration array
     * @return array default configuration
     */
    protected function defaultConfig()
    {
        return [
            'client_credentials' => [
                'class' => 'League\OAuth2\Server\Grant\ClientCredentialsGrant',
            ],
            'password' => [
                'class' => 'League\OAuth2\Server\Grant\PasswordGrant',
                'params' => function ($module, $repos) {
                    return [
                        $repos->getUserRepository(),
                        $repos->getRefreshTokenRepository(),
                    ];
                },
            ],
            'authorization_code' => [
                'class' => 'League\OAuth2\Server\Grant\AuthCodeGrant',
                'params' => function ($module, $repos) {
                    $authCodeTTL = $module->authCodeTTL ? $module->authCodeTTL : 'PT10M';
                    $params = [
                        $repos->getAuthCodeRepository(),
                        $repos->getRefreshTokenRepository(),
                        new \DateInterval($authCodeTTL)
                    ];
                    return $params;
                },
            ],
            'implicit' => [
                'class' => 'League\OAuth2\Server\Grant\ImplicitGrant',
                'params' => function ($module, $repos) {
                    $accessTokenTTL = $module->accessTokenTTL ? $module->accessTokenTTL : 'PT1H';
                    return [
                        new \DateInterval($accessTokenTTL),
                    ];
                },
            ],
            'refresh_token' => [
                'class' => 'League\OAuth2\Server\Grant\RefreshTokenGrant',
                'params' => function ($module, $repos) {
                    return [$repos->getRefreshTokenRepository()];
                }
            ],
        ];
    }

    /**
     * Returns the parameters passed to the constructor of grant class.
     * The parameters for all grant classes are returned if the $name is omitted.
     * @param  string|null $name grant type name
     * @return array parameters. An empty array is returned if the grant name does't exist.
     */
    public function getParams($name = null)
    {
        if (!$name) {
            return $this->_params;
        } elseif (isset($this->_params[$name])) {
            return $this->_params[$name];
        } else {
            return [];
        }
    }

    /**
     * Returns an instance of the grant class specified with grant type name
     * @param  string $name grant type name
     * @return object       an instance of the requested grant class
     * @see [[yii\di\container::get]]
     */
    public function getGrant($name)
    {
        return $this->get($name, $this->getParams($name));
    }
}
