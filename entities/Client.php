<?php

namespace mymth\oauth2server\entities;

use Yii;
use League\OAuth2\Server\Entities\ClientEntityInterface;

/**
 * Entity class for client / model class for "oauth_client" table.
 *
 * @property integer $id
 * @property string $secret
 * @property string $name
 * @property string $redirect_uri
 * @property integer $created_at
 * @property integer $updated_at
 */
class Client extends \yii\db\ActiveRecord implements ClientEntityInterface
{
    use EntityTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oauth_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['secret', 'default', 'value' => function ($model) {
                return $model->generateSecret();
            }],
            [['secret'], 'required'],
            [['redirect_uri', 'name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'secret' => 'Secret',
            'name' => 'Name',
            'redirect_uri' => 'Redirect URI',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
        ];
    }

    /**
     * Get the client's name.
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Returns the registered redirect URI (as a string).
     *
     * Alternatively return an indexed array of redirect URIs.
     *
     * @return string|string[]
     */
    public function getRedirectUri() {
        return $this->redirect_uri;
    }

    /**
     * Generates a random string for secret
     */
    public function generateSecret()
    {
        return Yii::$app->getSecurity()->generateRandomString();
    }
}
