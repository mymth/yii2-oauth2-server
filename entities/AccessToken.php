<?php

namespace mymth\oauth2server\entities;

use Yii;
use yii\behaviors\TimestampBehavior;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\Traits\AccessTokenTrait;

/**
 * Entity class for access token / model class for "oauth_access_token" table.
 *
 * @property string $id
 * @property integer $client_id
 * @property integer $user_id
 * @property integer $expire_time
 * @property string $scopes
 * @property integer $revoked
 * @property integer $created_at
 * @property integer $updated_at
 */
class AccessToken extends \yii\db\ActiveRecord implements AccessTokenEntityInterface
{
    use AccessTokenTrait, EntityTrait, TokenTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oauth_access_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'expire_time'], 'required'],
            [['client_id', 'user_id', 'expire_time'], 'integer'],
            [['scopes'], 'string'],
            [['id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'user_id' => 'User ID',
            'expire_time' => 'Expire Time',
            'scopes' => 'Scopes',
            'revoked' => 'Revoked',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
        ];
    }
}
