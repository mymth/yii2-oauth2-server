<?php

namespace mymth\oauth2server\entities;

use Yii;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;

/**
 * Entity class for refresh token / model class for "oauth_refresh_token" table.
 *
 * @property string $id
 * @property string $access_token_id
 * @property integer $expire_time
 * @property integer $revoked
 * @property integer $created_at
 * @property integer $updated_at
 */
class RefreshToken extends \yii\db\ActiveRecord implements RefreshTokenEntityInterface
{
    use EntityTrait;

    private $_accessToken;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oauth_refresh_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'access_token_id', 'expire_time'], 'required'],
            [['expire_time'], 'integer'],
            [['id', 'access_token_id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_token_id' => 'Access Token',
            'expire_time' => 'Expire Time',
            'revoked' => 'Revoked',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
        ];
    }

    /**
     * Get the token's expiry date time.
     *
     * @return \DateTime
     */
    public function getExpiryDateTime()
    {
        return (new \DateTime())->setTimestamp($this->expire_time);
    }

    /**
     * Set the date time when the token expires.
     *
     * @param \DateTime $dateTime
     */
    public function setExpiryDateTime(\DateTime $dateTime)
    {
        $this->expire_time = $dateTime->getTimestamp();
    }

    /**
     * Get the access token that the refresh token was originally associated with.
     *
     * @return AccessTokenEntityInterface
     */
    public function getAccessToken()
    {
        if ($this->access_token_id) {
            if (!$this->_accessToken || $this->_accessToken->getIdentifier() != $this->access_token_id) {
                $this->_accessToken = AccessToken::findOne($this->access_token_id);
            }
        } elseif ($this->_accessToken) {
            $this->_accessToken = null;
        }

        return $this->_accessToken;
    }

    /**
     * Set the access token that the refresh token was associated with.
     *
     * @param AccessTokenEntityInterface $accessToken
     */
    public function setAccessToken(AccessTokenEntityInterface $accessToken)
    {
        $this->_accessToken = $accessToken;
        $this->access_token_id = $accessToken->getIdentifier();
    }
}
