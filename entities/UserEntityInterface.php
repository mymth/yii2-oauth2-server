<?php

namespace mymth\oauth2server\entities;

/**
 * Interface that must be implemented by user entity class.
 */
interface UserEntityInterface extends \League\OAuth2\Server\Entities\UserEntityInterface
{
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username);

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password);
}
