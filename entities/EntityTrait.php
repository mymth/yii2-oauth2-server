<?php

namespace mymth\oauth2server\entities;

use yii\behaviors\TimestampBehavior;

/**
 * Trait implementing common functionality of oauth2 entitiy.
 */
trait EntityTrait
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->id;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->id = $identifier;
    }
}
