<?php

namespace mymth\oauth2server\entities;

use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\Traits\EntityTrait;

/**
 * Entity class for scope.
 **/
class Scope extends \yii\base\Object implements ScopeEntityInterface
{
    use EntityTrait;

    /**
     * @inheritdoc
     */
    public function __construct($config = null)
    {
        if (is_string($config)) {
            $config = ['identifier' => $config];
        }
        parent::__construct($config);
    }

    /**
     * @return string indentifier
     */
    public function jsonSerialize() {
        return $this->identifier;
    }
}
