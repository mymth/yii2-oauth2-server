<?php

namespace mymth\oauth2server\entities;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;

/**
 * Trait implementing common functionality of oauth2 token.
 */
trait TokenTrait
{
    private $_client;

    /**
     * @return \DateTime
     */
    public function getExpiryDateTime()
    {
        return (new \DateTime())->setTimestamp($this->expire_time);
    }

    /**
     * Set the date time when the token expires.
     *
     * @param \DateTime $dateTime
     */
    public function setExpiryDateTime(\DateTime $dateTime)
    {
        $this->expire_time = $dateTime->getTimestamp();
    }

    /**
     * @return string|int
     */
    public function getUserIdentifier()
    {
        return $this->user_id;
    }

    /**
     * @param string|int $identifier The identifier of the user
     */
    public function setUserIdentifier($identifier)
    {
        $this->user_id = $identifier;
    }

    /**
     * @return ClientEntityInterface
     */
    public function getClient()
    {
        if ($this->client_id) {
            if (!$this->_client || $this->_client->getIdentifier() != $this->client_id) {
                $this->_client = Client::findOne($this->client_id);
            }
        } elseif ($this->_client) {
            $this->_client = null;
        }

        return $this->_client;
    }

    /**
     * @param ClientEntityInterface $client
     */
    public function setClient(ClientEntityInterface $client)
    {
        $this->_client = $client;
        $this->client_id = $client->getIdentifier();
    }

    /**
     * @return ScopeEntityInterface[]
     */
    public function getScopes()
    {
        $scopes = [];
        if ($this->scopes) {
            foreach (explode(' ', $this->scopes) as $scopeName) {
                $scopes[] = new Scope($scopeName);
            }
        }

        return $scopes;
    }

    /**
     * @param ScopeEntityInterface $scope
     */
    public function addScope(ScopeEntityInterface $scope)
    {
        $scopes = $this->scopes ? explode(' ', $this->scopes) : [];
        $newScope = $scope->getIdentifier();
        if (!in_array($newScope, $scopes)) {
            $scopes[] = $newScope;
        }
        $this->scopes = $scopes ? implode(' ', $scopes) : null;
    }
}
