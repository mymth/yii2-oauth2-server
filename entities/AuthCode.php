<?php

namespace mymth\oauth2server\entities;

use Yii;
use League\OAuth2\Server\Entities\AuthCodeEntityInterface;

/**
 * Entity class for authorization code / model class for "oauth_auth_code" table.
 *
 * @property string $id
 * @property integer $client_id
 * @property integer $user_id
 * @property integer $expire_time
 * @property string $scopes
 * @property integer $revoked
 * @property integer $created_at
 * @property integer $updated_at
 */
class AuthCode extends \yii\db\ActiveRecord implements AuthCodeEntityInterface
{
    use EntityTrait, TokenTrait;

    private $_client;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oauth_auth_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'user_id'], 'required'],
            [['client_id', 'user_id', 'expire_time'], 'integer'],
            [['scopes'], 'string'],
            [['id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'user_id' => 'User ID',
            'expire_time' => 'Expire Time',
            'scopes' => 'Scopes',
            'revoked' => 'Revoked',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
        ];
    }

    /**
     * unused
     * @return null
     */
    public function getRedirectUri() {}

    /**
     * unused
     * @param string $uri
     */
    public function setRedirectUri($uri) {}
}
