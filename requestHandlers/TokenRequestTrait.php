<?php

namespace mymth\oauth2server\requestHandlers;

use League\OAuth2\Server\Exception\OAuthServerException;

/**
 * Trait that implements a method to process access token request sent to token endpoint.
 * The trait must be used with `mymth\oauth2server\psr7\Psr7BridgeTrait`.
 */
trait TokenRequestTrait
{
    /**
     * Processes access token request
     * @param  mymth\oauth2server\Module $module instance of the oauth2 server module
     * @return string the response body
     */
    protected function handleAccessTokenRequest($module)
    {
        $server = $module->getAuthorizationServer();
        $request = $this->getPsr7ServerRequest();
        $response = $this->getPsr7Response();

        try {
            $response = $server->respondToAccessTokenRequest($request, $response);
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
        }

        return $this->applyResponse($response);
    }
}
