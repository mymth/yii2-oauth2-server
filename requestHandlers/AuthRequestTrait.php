<?php

namespace mymth\oauth2server\requestHandlers;

use Yii;
use League\OAuth2\Server\Exception\OAuthServerException;

/**
 * Trait that implements a method to process authorization request sent to authorize
 * endpoint.
 * The trait must be used with `mymth\oauth2server\psr7\Psr7BridgeTrait`.
 */
trait AuthRequestTrait
{
    /**
     * Processes authorize request
     * @param  mymth\oauth2server\Module $module instance of the oauth2 server module
     * @return mixed null when user needs to login, an AuthorizationRequest instance
     * when user needs to authorize the client, a string for response body when user
     * has authorized the client and false when user has canceled the authorization
     * @throws yii\web\HttpException if the request is invalid
     */
    protected function handleAuthorizeRequest($module)
    {
        $server = $module->getAuthorizationServer();
        $request = $this->getPsr7ServerRequest();
        $response = $this->getPsr7Response();

        $session = Yii::$app->getSession();
        $webUser = Yii::$app->getUser();

        $authRequest = $session->get('authRequest');
        try {
            if (!$authRequest) {
                $authRequest = $server->validateAuthorizationRequest($request);
                $session->set('authRequest', $authRequest);
            }
            if ($webUser->getIsGuest()) {
                $webUser->loginRequired();
                return;
            } else {
                $authRequest->setUser($webUser->getIdentity());

                $authorized = Yii::$app->getRequest()->post('authorized');
                if ($authorized === null) {
                    return $authRequest;
                } elseif (!$authorized) {
                    $session->remove('authRequest');
                    return false;
                }

                $authRequest->setAuthorizationApproved(true);
                $response = $server->completeAuthorizationRequest($authRequest, $response);

                $session->remove('authRequest');
            }
        } catch (\Exception $exception) {
            $session->remove('authRequest');
            if ($exception instanceof OAuthServerException) {
                $statusCode = $exception->getHttpStatusCode();
                $message = $exception->getMessage();
                throw new \yii\web\HttpException($statusCode, $message);
            }
            throw $exception;
        }

        return $this->applyResponse($response);
    }
}
