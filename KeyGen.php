#!/usr/bin/env php
<?php

class KeyGen
{
    /**
     * @var string $priKeyPath path to the private key file
     * absolute path or relative path from the application's root
     */
    public $privateKey = 'runtime/oauth2/private.key';
    /**
     * @var string $pubKeyPath path to the public key file
     * absolute path or relative path from the application's root
     */
    public $publicKey = 'runtime/oauth2/public.key';

    private $appRootPath;

    /**
     * Creates a private-public key pair using PHP's OpenSSL functions
     * The destination directory is created automatically if not exist.
     * @throws Exception if there's a problem with the paths
     */
    public function create()
    {
        $priKeyPath = $this->getFullKeyPath($this->privateKey);
        $pubKeyPath = $this->getFullKeyPath($this->publicKey);
        if ($priKeyPath == $pubKeyPath) {
            throw new \Exception('Paths to the private and public keys cannot be the same');
        }

        $priKeyDir = dirname($priKeyPath);
        $pubKeyDir = dirname($pubKeyPath);
        $this->prepareKeyDir($priKeyDir);
        if ($pubKeyDir != $priKeyDir) {
            $this->prepareKeyDir($pubKeyDir);
        }

        // generate a new private key
        $pkey = openssl_pkey_new(['private_key_bits' => 1024]);
        // get a string expression of the private key
        openssl_pkey_export($pkey, $privateKey);

        // get the corresponding public key
        $details = openssl_pkey_get_details($pkey);
        $publicKey = $details['key'];

        // write the keys to individual files
        file_put_contents($priKeyPath, $privateKey);
        file_put_contents($pubKeyPath, $publicKey);

        echo "--\n";
        echo "Key files have been created.\n";
        echo "Private key: " . realpath($priKeyPath) . "\n";
        echo "Public key : " . realpath($pubKeyPath) . "\n";
        echo "\n";
    }

    /**
     * Returns the root directory path of the application.
     * The method searches for this file's ancester directory that has the `vendor`
     * directory in its children and determins it as the app's root directory.
     * @return string path to the app's root directory
     */
    public function getAppRootPath()
    {
        if (!$this->appRootPathp) {
            $path = dirname(__DIR__);
            do {
                if (is_dir($path . '/vendor')) {
                    $this->appRootPath = $path;
                    break;
                }
                $path = dirname($path);
            } while ($path != '/');

            if (!$this->appRootPath) {
                throw new \Exception('Could not find the app\'s root directory.');
            }
        }

        return $this->appRootPath;
    }

    public function setAppRootPath($path)
    {
        $this->appRootPath = $path;
    }

    protected function getFullKeyPath($keyPath)
    {
        if (substr($keyPath, 0, 1) != '/') {
            $keyPath = $this->appRootPath . '/' . $keyPath;
        }

        return $keyPath;
    }

    protected function prepareKeyDir($keyDir)
    {
        $workDir = '';
        $dir = strtok($keyDir, '/');
        while ($dir !== false) {
            $workDir .= '/' . $dir;
            if (is_file($workDir)) {
                throw new \Exception('Invalid key path: ' . $workDir . ' is a file.');
            } elseif (!is_dir($workDir)) {
                mkdir($workDir);
            }
            $dir = strtok('/');
        }
    }
}

$keyGen = new KeyGen();
try {
    $appRoot = $keyGen->getAppRootPath();
} catch (\Exception $e) {
    echo 'ERROR: ' . $e->getMessage() . "\n";
    exit(1);
}
$cwd = getcwd();

$stdin = fopen('php://stdin', 'r');
if ($appRoot != $cwd) {
    echo "Is $appRoot your app's root directory? [Y/n]: ";
    $ans = trim(fgets($stdin));
    if ($ans && !preg_match('/^y/i', $ans)) {
        $newAppRoot = false;
        while ($newAppRoot === false) {
            echo "Enter the app root directory: ";
            $newAppRoot = trim(fgets($stdin));
            if (!$newAppRoot) {
                $newAppRoot = false;
                continue;
            }
            if (substr($newAppRoot, 0, 1) != '/') {
                $newAppRoot = $cwd . '/' . $newAppRoot;
            }
            if (!is_dir($newAppRoot)) {
                echo "$newAppRoot is not a directory!\n";
                $newAppRoot = false;
            }
        }
        if ($newAppRoot) {
            $keyGen->setAppRootPath($newAppRoot);
            echo "root directory changed: " . realpath($newAppRoot) . "\n";
            echo  "--\n";
        }
    }
}
echo "Private key file path? [$keyGen->privateKey]: ";
$newKeyPath = trim(fgets($stdin));
if ($newKeyPath) {
    $keyGen->privateKey = $newKeyPath;
}
echo "Public key file path? [$keyGen->publicKey]: ";
$newKeyPath = trim(fgets($stdin));
if ($newKeyPath) {
    $keyGen->publicKey = $newKeyPath;
}
fclose($stdin);

try {
    $keyGen->create();
} catch (\Exception $e) {
    echo 'ERROR: ' . $e->getMessage() . "\n";
    exit(1);
}
exit;
