<?php

namespace mymth\oauth2server\filters;

use Yii;

/**
 * Access rule for [[yii\filters\AccessControl]] action filter that supports Oauth2 scopes.
 *
 * When the [[$scopes]] property is not empty, the class checks the `oauth_scopes`
 * attribute of the application paramter (`\Yii::$app->params`) specified by
 * [[$requestAttributesParam]], which is set by [[HttpBearerAuth]], against it.
 *
 * To use this class, you can assign it to the `ruleConfig` of the `access` behavior
 * of a controller or module
 * ```php
 * public function behaviors()
 * {
 *     $behaviors = parent::behaviors();
 *     $behaviors['access'] = [
 *          'class' => 'yii\filters\AccessControl',
 *          'ruleConfig' => ['class' => 'mymth\oauth2server\filters\AccessRule'],
 *          'rules' => [
 *              [
 *                  'allow' => true,
 *                  'actions' => ['foo'],
 *                  'scopes' => ['foo', 'bar'],
 *              ],
 *          ],
 *      ];
 *
 *     return $behaviors;
 * }
 * ```
 */
class AccessRule extends \yii\filters\AccessRule
{
    /**
     * @var array list of oauth2 scopes that this rule applies to.
     * If not set or empty, it means this rule applies to all scopes.
     */
    public $scopes;
    /**
     * @var string application parameter name for oauth request attributes
     */
    public $requestAttributesParam = 'requestAttributes';

    /**
     * @inheritdoc
     */
    public function allows($action, $user, $request)
    {
        $params = Yii::$app->params;
        $raName = $this->requestAttributesParam;
        if (isset($params[$raName]) && isset($params[$raName]['oauth_scopes'])) {
            $scopes = $params[$raName]['oauth_scopes'];
        } else {
            $scopes = null;
        }

        if ($this->matchScope($scopes)) {
            return parent::allows($action, $user, $request);
        }

        return;
    }

    /**
     * @param array $scopes scopes
     * @return boolean whether the rule applies to the scope
     */
    protected function matchScope($scopes)
    {
        if (!$this->scopes) {
            return true;
        }
        if ($scopes) {
            if (array_intersect($scopes, $this->scopes)) {
                return true;
            }
        }

        return false;
    }
}
