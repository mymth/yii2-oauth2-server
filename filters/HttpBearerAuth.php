<?php

namespace mymth\oauth2server\filters;

use Yii;
use yii\base\InvalidConfigException;
use League\OAuth2\Server\ResourceServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use mymth\oauth2server\psr7\Psr7BridgeTrait;
use mymth\oauth2server\repositories\AccessTokenRepository;

/**
 * Action filter that supports the authentication method based on HTTP Bearer token.
 *
 * When the access token is valid, an array of the following attributes are set into
 * the application paramter (`\Yii::$app->params`) with the name specified in
 * [[$requestAttributesParam]]
 *
 *   - `oauth_access_token_id` - the access token identifier
 *   - `oauth_client_id` - the client identifier
 *   - `oauth_user_id` - the user identifier represented by the access token
 *   - `oauth_scopes` - an array of string scope identifiers
 *
 * To use this filter, you can attach it as a behavior to a controller or module
 * ```php
 * public function behaviors()
 * {
 *     $behaviors = parent::behaviors();
 *     $behaviors['authenticator'] =>[
 *         'class' => 'mymth\oauth2server\filters\HttpBearerAuth',
 *     ];
 *
 *     return $behaviors;
 * }
 * ```
 */
class HttpBearerAuth extends \yii\filters\auth\HttpBearerAuth
{
    use Psr7BridgeTrait;

    /**
     * @var string path to public key file
     */
    public $publicKey = '@runtime/oauth2/public.key';
    /**
     * @var string application parameter name for oauth request attributes
     */
    public $requestAttributesParam = 'requestAttributes';

    private $_accessTokenRepository;

    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response)
    {
        $rsrcServer = $this->getResourceServer();
        $serverRequest = $this->getPsr7ServerRequest();

        try {
            $serverRequest = $rsrcServer->validateAuthenticatedRequest($serverRequest);
        } catch (\Exception $e) {
            $this->handleFailure($response);
        }
        $attributes = $serverRequest->getAttributes();
        Yii::$app->params[$this->requestAttributesParam] = $attributes;

        $accessToken = $attributes['oauth_access_token_id'];
        $identity = $user->loginByAccessToken($accessToken, get_class($this));
        if ($identity === null) {
            $this->handleFailure($response);
        }

        return $identity;
    }

    /**
     * Returns an instance of AccessTokenRepository
     * @return AccessTokenRepositoryInterface instance of AccessTokenRepository
     */
    public function getAccessTokenRepository()
    {
        if (!$this->_accessTokenRepository) {
            $this->setAccessTokenRepository(['class' => AccessTokenRepository::class]);
        }

        return $this->_accessTokenRepository;
    }

    /**
     * Sets an instance of AccessTokenRepository
     * @param AccessTokenRepositoryInterface|array $repository instance of
     * AccessTokenRepository or a configuration array for AccessTokenRepository
     */
    public function setAccessTokenRepository($repository)
    {
        if (!$repository instanceof AccessTokenRepositoryInterface) {
            $repository = Yii::createObject($repository);
            if (!$repository instanceof AccessTokenRepositoryInterface) {
                $msg = get_class($repository) . ' must implement '
                    . AccessTokenRepositoryInterface::class;
                throw new InvalidConfigException($msg);
            }
        }
        $this->_accessTokenRepository = $repository;
    }

    protected function getResourceServer()
    {
        return new ResourceServer(
            $this->getAccessTokenRepository(),
            Yii::getAlias($this->publicKey)
        );
    }
}
