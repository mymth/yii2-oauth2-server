<?php

use yii\db\Migration;

/**
 * Initializes oauth tables
 */
class m161218_185845_oauth2_init extends Migration
{
    public function up()
    {
        $this->createTable('oauth_auth_code', [
            'id' => $this->string()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'expire_time' => $this->bigInteger(),
            'scopes' => $this->text(),
            'revoked' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
            'PRIMARY KEY (id)',
        ]);

        $this->createTable('oauth_access_token', [
            'id' => $this->string()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'expire_time' => $this->bigInteger()->notNull(),
            'scopes' => $this->text(),
            'revoked' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
            'PRIMARY KEY (id)',
        ]);

        $this->createTable('oauth_refresh_token', [
            'id' => $this->string()->notNull(),
            'access_token_id' => $this->string()->notNull(),
            'expire_time' => $this->integer()->notNull(),
            'revoked' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
            'PRIMARY KEY (id)',
        ]);
        $this->addForeignKey('fk-oauth_refresh_token-access_token', 'oauth_refresh_token', 'access_token_id', 'oauth_access_token', 'id', 'CASCADE');

        $this->createTable('oauth_client', [
            'id' => $this->primaryKey(),
            'secret' => $this->string()->notNull(),
            'name' => $this->string(),
            'redirect_uri' => $this->text(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('oauth_client');
        $this->dropTable('oauth_refresh_token');
        $this->dropTable('oauth_access_token');
        $this->dropTable('oauth_auth_code');
    }
}
