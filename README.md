# yii2-oauth2-server

Yii2 wrapper for [league/oauth2-server](https://oauth2.thephpleague.com)

## Installation

Add the git repo of this extension into your application's `composer.json`.
```json
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:mymth/yii2-oauth2-server.git"
    }
],
```

Then, install through composer
```
php composer.phar require mymth/yii2-oauth2-server
```

#### Setting up database

Run Yii's `migrate` command.
```
yii migrate --migrationPath=@vendor/mymth/yii2-oauth2-server/migrations
```

#### Creating private and public keys

Run the `KeyGen` script in the extension.
```sh
php vendor/mymth/yii2-oauth2-server/KeyGen.php
```

The script asks you the paths to create the files. If you want to use different locations from the default, enter them with relative path from your application's root or absolute path.
```
Private key file path? [runtime/oauth2/private.key]: 
Public key file path? [runtime/oauth2/public.key]: 
```

Then, the script creates the key files. (_It automatically complements non-existing directories in the path._)

## Authorization Server

#### Preparation

Implement `mymth\oauth2server\entities\UserEntityInterface` on your identity class.

#### Configuration

Add this extension's Module into the `modules` secion of your application's config file (`config/web.php`). You must define the grant types to use in `availableGrantTypes`.
```php
//...
'modules' => [
    'oauth2' => [
        'class' => 'mymth\oauth2server\Module',
        'availableGrantTypes' = ['password', 'authorization_code', 'refresh_token'],
    ]
    //...
],
//...
```

**Options**

Name|Type|Default|Description
---|---|---|---
**`availableGrantTypes`**|string[]|`[]`|Grant types to enable on the server. The following types can be used: `client_credentials`, `password`, `authorization_code`, `implicit`, `refresh_token`
**`availableScopes`**|string[]|`[]`|Scopes available on the server.
**`privateKey`**|string|`@runtime/oauth2/private.key`|Path to private key file
**`publicKey`**|string|`@runtime/oauth2/public.key`|Path to public key file
**`accessTokenTTL`**|string\|null|`null`|TTL for access token in ISO8601 duration notation. When not set, `PT1H` is used
**`authCodeTTL`**|string\|null|`null`|TTL for authorization code in ISO8601 duration notation. When not set, `PT10M` is used
**`refreshTokenTTL`**|string\|null|`null`|TTL for refresh token in ISO8601 duration notation. When not set, `P1M` is used

#### Token endpoint

1. Create your own controller and action for the token endpoint.
2. In the controller (or the action if you create it as a class), use the following traits:

    - `mymth\oauth2server\psr7\Psr7BridgeTrait`
    - `mymth\oauth2server\requestHandlers\TokenRequestTrait`

3. In the action method, invoke `handleAccessTokenRequest()` passing the instance of this server module.
4. Return the result of `handleAccessTokenRequest()`.

```php
//...
use mymth\oauth2server\psr7\Psr7BridgeTrait;
use mymth\oauth2server\requestHandlers\TokenRequestTrait;

class YourOauthController extends Controller
{
    use Psr7BridgeTrait, TokenRequestTrait;

    //...

    public function actionToken()
    {
        $module = \Yii::$app->getModule('oauth2');

        retunr $this->handleAccessTokenRequest($module);
    }

    //...
}
```

#### Authorize endpoint

1. Create your own controller, action and view for the authorize endpoint.
2. In the controller (or the action if you create it as a class), use the following traits:

    - `mymth\oauth2server\psr7\Psr7BridgeTrait`
    - `mymth\oauth2server\requestHandlers\AuthRequestTrait`

3. In the action method, invoke `handleAuthorizeRequest()` passing the instance of this server module.  
   The result of `handleAuthorizeRequest()` indicates which authorization phase the request is in. The action method has to take one of the followings depending on the result.

    The result|Meaning|Next action
    ---|---|---
    Instance of `AuthorizationRequest`|The user has logged in and needs to authorize the client (_the login process is handled by `handleAuthorizeRequest()` internally_)|Render the authorize view then return it
    `false`|The user has refused to authorize the client|Your own action in this case (e.g. redirecting to "canceled" page, etc.)
    Other than above (string for response body)|The user has authorized the client and the server needs to redirect the user back to the client.|Return the result. (_the response header and status for redirection are already set by `handleAuthorizeRequest()` internally_)

4. In the view, create a form to submit the `authorized` parameter to the authorize endpoint using `POST` method.  
   When the user authorizes the client, `authorized` must be sent with a truthy value, and a falsy value in the opposit case.

**Controller**
```php
//...
use mymth\oauth2server\psr7\Psr7BridgeTrait;
use mymth\oauth2server\requestHandlers\AuthRequestTrait;

class YourOauthController extends Controller
{
    use Psr7BridgeTrait, AuthRequestTrait;

    //...

    public function actionAuthorize()
    {
        $module = \Yii::$app->getModule('oauth2');

        $result = $this->handleAccessTokenRequest($module);
        if (is_object($resut)) {
            return $this->render('authorize', ['authRequest' => $result]);
        } elseif ($result === false) {
            $this->redirect('oauth/cancelled');
        }

        return $result;
    }

    //...
}
```
**View**
```php
//...
<?= Html::beginForm('', 'post', ['id' => 'authorize-form']) ?>
    <?= Html::hiddenInput('authorized', null, ['id' => 'authorized']) ?>
    <div class="form-group">
        <?= Html::button('Authorize', ['data' => ['authval' => 1]]) ?>
        <?= Html::button('Cancel', ['data' => ['authval' => 0]]) ?>
    </div>
<?= Html::endForm() ?>
<script>
$(document).ready(function () {
    $('#authorize-form button').on('click', function (evt) {
        evt.preventDefault();
        $('#authorized').val($(evt.target).data('authval'));
        $('#authorize-form').submit();
    });
});
</script>
```

## Resource Server

#### Preparation

Implement the `findIdentityByAccessToken()` method in your identity class.

#### Authenticator

Configure your controller's `authenticator` behavior with `mymth\oauth2server\filters\HttpBearerAuth`.

```php
class YourApiController extends \yii\rest\Controller
{
    //...

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => 'mymth\oauth2server\filters\HttpBearerAuth',
        ];
        //...

        return $behaviors;
    }

    //...
}
```

`mymth\oauth2server\filters\HttpBearerAuth` is a subclass of `yii\filters\auth\HttpBearerAuth`. In addition to authenticating through HTTP Bearer token, the class sets the following attributes onto the application paramter `\Yii::$app->params['requestAttributes']` when the access token is valid:

- `oauth_access_token_id` - the access token identifier
- `oauth_client_id` - the client identifier
- `oauth_user_id` - the user identifier represented by the access token
- `oauth_scopes` - an array of string scope identifiers

**Options**

Name|Type|Default|Description
---|---|---|---
`publicKey`|string|`@runtime/oauth2/public.key`|Path to public key file
`requestAttributesParam`|string|`requestAttributes`|Application parameter name for oauth request attributes
`accessTokenRepository`|array|[class => AccessTokenRepository]|Config array for access token repository
> See [yii\filters\auth\HttpBearerAuth](http://www.yiiframework.com/doc-2.0/yii-filters-auth-httpbearerauth.html) for the inherited options.

#### Access Controll

If you like to use scopes for access controll, you can configure your controller's `access` behavior with `mymth\oauth2server\filters\AccessRule`

```php
class YourApiController extends \yii\rest\Controller
{
    //...

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //...

        $behaviors['access'] = [
            'class' => 'yii\filters\AccessControl',
            'ruleConfig' => ['class' => 'mymth\oauth2server\filters\AccessRule'],
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['foo'],
                    'scopes' => ['foo', 'bar'],
                ],
                //...
            ],
        ];

        return $behaviors;
    }

    //...
}
```

`mymth\oauth2server\filters\AccessRule` extends `yii\filters\AccessRule` to allow users to use scopes as rule criteria. It checkes the scopes set on the applicaton parameter `\Yii::$app->params['requestAttributes']['oauth_scopes']` against the `$scopes` property. If the `$scopes` property is empty, scopes are excluded from the rule criteria.

**Options**

Name|Type|Default|Description
---|---|---|---
`scopes`|array|`[]`|Scopes the rule applies to
`requestAttributesParam`|string|`requestAttributes`|Application parameter name for oauth request attributes
> See [yii\filters\AccessRule](http://www.yiiframework.com/doc-2.0/yii-filters-accessrule.html) for the inherited options.
