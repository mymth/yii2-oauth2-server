<?php

namespace mymth\oauth2server;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * DI container that provides a singleton instance of each repositry class.
 */
class RepositoryCollection extends \yii\di\Container
{
    private $_depsConf = [];

    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        $repoKeys = array_keys($this->defaultConfig());
        foreach (array_keys($config) as $key) {
            if (in_array($key, $repoKeys)) {
                $cf = $config[$key];
                $this->_depsConf[$key] = is_array($cf) ? $cf : ['class' => $cf];
                unset($config[$key]);
            }
        }

        parent::__construct($config);
    }

    public function init()
    {
        $depsConf = ArrayHelper::merge($this->defaultConfig(), $this->_depsConf);
        foreach ($depsConf as $key => $cf) {
            $this->setSingleton($key, $cf);
        }
    }

    /**
     * Returns the default configuration array
     * @return array default configuration
     */
    protected function defaultConfig() {
        return [
            'accessTokenRepository' => [
                'class' => 'mymth\oauth2server\repositories\AccessTokenRepository',
            ],
            'authCodeRepository' => [
                'class' => 'mymth\oauth2server\repositories\AuthCodeRepository',
            ],
            'clientRepository' => [
                'class' => 'mymth\oauth2server\repositories\ClientRepository',
            ],
            'refreshTokenRepository' => [
                'class' => 'mymth\oauth2server\repositories\RefreshTokenRepository',
            ],
            'scopeRepository' => [
                'class' => 'mymth\oauth2server\repositories\ScopeRepository',
            ],
            'userRepository' => [
                'class' => 'mymth\oauth2server\repositories\UserRepository',
            ],
        ];
    }

    /**
     * Returns an instance of AccessTokenRepository
     * @return AccessTokenRepositoryInterface AccessTokenRepository
     */
    public function getAccessTokenRepository()
    {
        return $this->get('accessTokenRepository');
    }

    /**
     * Returns an instance of AuthCodeRepository
     * @return AuthCodeRepositoryInterface AuthCodeRepository
     */
    public function getAuthCodeRepository()
    {
        return $this->get('authCodeRepository');
    }

    /**
     * Returns an instance of ClientRepository
     * @return ClientRepositoryInterface ClientRepository
     */
    public function getClientRepository()
    {
        return $this->get('clientRepository');
    }

    /**
     * Returns an instance of RefreshTokenRepository
     * @return RefreshTokenRepositoryInterface RefreshTokenRepository
     */
    public function getRefreshTokenRepository()
    {
        return $this->get('refreshTokenRepository');
    }

    /**
     * Returns an instance of ScopeRepository
     * @return ScopeRepositoryInterface ScopeRepository
     */
    public function getScopeRepository()
    {
        return $this->get('scopeRepository');
    }

    /**
     * Returns an instance of UserRepository
     * @return UserRepositoryInterface UserRepository
     */
    public function getUserRepository()
    {
        return $this->get('userRepository');
    }
}
