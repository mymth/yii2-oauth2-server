<?php

namespace mymth\oauth2server\psr7;

use Yii;
use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Psr7\Response;

/**
 * Trait that provides methods to mediate PSR-7 ServerRequest/Response and Yii
 * application's Request/Response.
 */
trait Psr7BridgeTrait
{
    /**
     * Creates an instance of PSR-7 ServerRequest from application's request object
     * @return GuzzleHttp\Psr7\ServerRequest PSR-7 server request
     */
    protected function getPsr7ServerRequest()
    {
        $appRequest = Yii::$app->getRequest();

        return (new ServerRequest(
                $appRequest->getMethod(),
                $appRequest->getUrl(),
                $appRequest->getHeaders()->toArray(),
                $appRequest->getRawBody(),
                $version = '1.1',
                $_SERVER
            ))
            ->withCookieParams($appRequest->getCookies()->toArray())
            ->withQueryParams($appRequest->getQueryParams())
            ->withParsedBody($appRequest->getBodyParams());
    }

    /**
     * Creates an instance of PSR-7 Response from application's response object
     * @return GuzzleHttp\Psr7\Response PSR-7 response
     */
    protected function getPsr7Response()
    {
        $appResponse = Yii::$app->getResponse();

        return new Response(
                $appResponse->getStatusCode(),
                $appResponse->getHeaders()->toArray()
            );
    }

    /**
     * Reflects the PSR-7 response to application's response and returns the
     * response body
     * @param  GuzzleHttp\Psr7\Response $response PSR-7 response
     * @return string the body of the PSR-7 response
     */
    protected function applyResponse(Response $response)
    {
        $appResponse = Yii::$app->getResponse();

        $appResponse->setStatusCode($response->getStatusCode());
        $appResponse->getHeaders()->fromArray($response->getHeaders());
        $appResponse->format = \yii\web\Response::FORMAT_RAW;

        return $response->getBody()->__toString();
    }
}
