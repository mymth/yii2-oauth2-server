<?php

namespace mymth\oauth2server;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\ResourceServer;

/**
 * OAuth2 authorization server module
 *
 * To define the grant types used on this module, set them in [[$availableGrantTypes]].
 * The following types can be used by default:
 * `client_credentials`, `password`, `authorization_code`, `implicit`, `refresh_token`
 *
 * To define the scopes available on this module, set them in [[$availableScopes]].
 *
 * The module uses the identity class defined in `\Yii::$app->user->identityClass`
 * as the default user entity class. If you like to customize it, set your own user
 * entity class into the config array like the following:
 * ```php
 * 'modules' => [
 *     'oauth2' => [
 *         'class' => 'mymth\oauth2server\Module',
 *         'repositories' => [
 *             'userRepository' => ['entityClass' => 'your\own\UserEntity'],
 *         ],
 *     ],
 * ]
 * ```
 *
 * If you like to use your custom grant type, first, create your custome grant type
 * class that implements `League\OAuth2\Server\Grant\GrantTypeInterface`. Then, set
 * its identifier and class name into the config array like the following:
 * ```php
 * 'modules' => [
 *     'oauth2' => [
 *         'class' => 'mymth\oauth2server\Module',
 *         'availableGrantTypes' => ['custom_grant'],
 *         'grantTypes' => [
 *             'custom_grant' => ['class' => 'your\own\GrantType'],
 *         ],
 *     ],
 * ]
 * ```
 */
class Module extends \yii\base\Module
{
    /**
     * @var array grant types to enable on the authorization server
     */
    public $availableGrantTypes = [];
    /**
     * @var array scopes available on the authorization server
     */
    public $availableScopes = [];
    /**
     * @var string path to private key file
     */
    public $privateKey = '@runtime/oauth2/private.key';
    /**
     * @var string path to public key file
     */
    public $publicKey = '@runtime/oauth2/public.key';
    /**
     * @var string|null TTL for access token in ISO8601 duration notation
     */
    public $accessTokenTTL;
    /**
     * @var string|null TTL for authorization code in ISO8601 duration notation
     */
    public $authCodeTTL;
    /**
     * @var string|null TTL for refresh token in ISO8601 duration notation
     */
    public $refreshTokenTTL;

    private $_authServer;
    private $_rsrcServer;

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, $config = [])
    {
        $compCf = isset($config['components']) ? $config['components'] : [];
        $this->prepareComp($compCf, 'repositories', RepositoryCollection::class);
        $this->prepareComp($compCf, 'grantTypes', GrantCollection::class);
        $config['components'] = $compCf;

        parent::__construct($id, $parent, $config);
    }

    /**
     * Returns authorization server
     * @return League\OAuth2\Server\AuthorizationServe authorization server
     */
    public function getAuthorizationServer()
    {
        if (!$this->_authServer) {
            $repos = $this->getRepositories();

            $server = new AuthorizationServer(
                $repos->getClientRepository(),
                $repos->getAccessTokenRepository(),
                $repos->getScopeRepository(),
                Yii::getAlias($this->privateKey),
                Yii::getAlias($this->publicKey)
            );

            $accessTokenTTL = $this->getDateIntervalFor('accessTokenTTL');
            $refreshTokenTTL = $this->getDateIntervalFor('refreshTokenTTL');
            foreach ($this->availableGrantTypes as $grantType) {
                $grant = $this->getGrantTypes()->getGrant($grantType);
                $methods = get_class_methods($grant);
                if (in_array('setRefreshTokenTTL', $methods) && $refreshTokenTTL) {
                    $grant->setRefreshTokenTTL($refreshTokenTTL);
                }
                $server->enableGrantType($grant, $accessTokenTTL);
            }

            $this->_authServer = $server;
        }

        return $this->_authServer;
    }

    /**
     * Returns the repository collection
     * @return mymth\oauth2server\RepositoryCollection the repository collection
     */
    public function getRepositories()
    {
        return $this->get('repositories');
    }

    /**
     * Returns the grant type collection
     * @return mymth\oauth2server\GrantCollection the grant type collection
     */
    public function getGrantTypes()
    {
        return $this->get('grantTypes');
    }

    /**
     * Processes the configuration for the specified component
     * @param  array  &$compCf   the `components` block of the configuration
     * @param  string $id        component id
     * @param  string $className class of the component
     */
    protected function prepareComp(&$compCf, $id, $className)
    {
        $config = isset($compCf[$id]) ? $compCf[$id] : [];
        if (!is_array($config)) {
            $config = ['class' => $className];
        } elseif (!isset($config['class'])) {
            $config['class'] = $className;
        }

        $compCf[$id] = $config;
    }

    /**
     * Returns an instance of DateInterval for TTL fields
     * @param  string $fieldName name of the TTL field
     * @return DateInterval instance of DateInterval for the TTL
     */
    protected function getDateIntervalFor($fieldName)
    {
        $ttl = $this->$fieldName;

        return $ttl ? new \DateInterval($ttl) : null;
    }
}
